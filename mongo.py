import logging
from datetime import datetime

logger = logging.getLogger('bizbots_vk_bot.bot')


class Messages(object):
    def __init__(self, db):
        self.db = db
        self.messages = db.messages

    def insert_entry(self, message):
        try:
            message['date'] = datetime.fromtimestamp(message['date'])
            self.messages.insert_one(message)
        except Exception as ex:
            logger.exception('Error logging message')
            return 0

class Users(object):
    def __init__(self, db):
        self.db = db
        self.users = db.users

    def insert_entry(self, user_dict):
        self.users.insert_one(user_dict)

    def get_user_by_id(self, user_id):
        user = self.users.find_one({'id': user_id})
        return user

    def get_turned_user_by_id(self, user_id, turn):
        user = self.users.find_one({'id': user_id, 'turn_on': turn})
        return user

    def get_turned_users(self, turn):
        user = self.users.find({'turn_on': turn})

        return user

    def set_turn(self, user_id, turn):
        try:
            self.users.update_one({'id': user_id}, {'$set': {'turn_on': turn}})
            return 1
        except Exception as ex:
            logger.exception('Error turning on/off bot for user')
            return 0
