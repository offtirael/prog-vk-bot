import json
import os
from datetime import datetime
import vk

import cherrypy
import pymongo
import redis
import pickle

from ordered_set import OrderedSet

from config import GROUP_ID, VERIFICATION_CODE, TOKEN, STATS_USER, STATS_PASS
from helpers import get_texts_from_pickle, russian_xml_data_to_dict
from mongo import Messages, Users

# Mongo data
connection = pymongo.MongoClient('127.0.0.1:27017')
from redis_cache import RedisCache

STATIC_ROOT_PATH = os.path.abspath(os.getcwd())
# connection = pymongo.MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
messages = Messages(connection.techmeskills_vk_bot)
users = Users(connection.techmeskills_vk_bot)
# Redis data
# red = redis.StrictRedis(os.environ['REDIS_PORT_6379_TCP_ADDR'])
red = redis.StrictRedis(host='localhost', db=3)
cache = RedisCache(red)


TEXTS = get_texts_from_pickle('data.pickle')


class BotServer(object):
    def __init__(self, bot):
        self.bot = bot

    @cherrypy.expose
    def index(self, vk_data):
        if vk_data:
            data = json.loads(vk_data)
            if not data:
                raise cherrypy.HTTPError(403)
            if data.get('type') == 'confirmation' and data.get('group_id') == GROUP_ID:
                return VERIFICATION_CODE
            elif data.get('type') == 'message_new':
                message = data.get('object')

                if not users.get_user_by_id(message['user_id']):
                    users.insert_entry({'id': message['user_id'], 'turn_on': True})
                if not cache.get_cache(message['user_id']):
                    cache.set_cache(message['user_id'], {'start': True})

                try:
                    if self.bot.turned_on:
                        result = self.bot.process_message(message, messages)
                    else:
                        result = 'no'
                except Exception:
                    return 'ok'
                return result
            return 'ok'
        else:
            raise cherrypy.HTTPError(403)

    @cherrypy.expose('stats')
    def stats(self):

        turned = 'включен' if self.bot.turned_on else 'отключен'
        users_for_html = []
        for user in users.get_turned_users(False):
            users_for_html.append("<tr><td>{}</td><td>{}</td></tr>".format(user['id'], 'Отключен'))

        users_for_html = "".join(users_for_html)

        context = {'turned': turned,
                   'users': users_for_html
                   }
        resp = '<b>Статистика на {:%Y-%m-%d %H:%M:%S}</b><br/>\n<br/>\n'.format(datetime.today())

        with open('templates/stats.html', 'r', encoding='utf-8') as file:
            resp += file.read().format(**context)

        return resp

    @cherrypy.expose('global_off')
    @cherrypy.tools.allow(methods=['POST'])
    def global_off(self, **kwargs):
        try:
            field = kwargs['field']
            if field == 'on':
                self.bot.turned_on = True
            elif field == 'off':
                self.bot.turned_on = False

        except Exception as e:
            raise cherrypy.HTTPRedirect("stats")

        raise cherrypy.HTTPRedirect("stats")

    @cherrypy.expose('off_user')
    @cherrypy.tools.allow(methods=['POST'])
    def off_user(self, **kwargs):

        try:
            user_id = int(kwargs['turnOffUser'])
            current_user = users.get_user_by_id(user_id)

            if current_user:
                users.set_turn(current_user['id'], False)

            else:
                users.insert_entry({'id': user_id, 'turn_on': False})
                cache.set_cache(user_id, {'start': True})

        except Exception as e:
            raise cherrypy.HTTPRedirect("stats")

        raise cherrypy.HTTPRedirect("stats")

    @cherrypy.expose('on_user')
    @cherrypy.tools.allow(methods=['POST'])
    def on_user(self, **kwargs):

        try:
            user_id = int(kwargs['turnOnUser'])
            current_user = users.get_user_by_id(user_id)

            if current_user:
                users.set_turn(current_user['id'], True)
                cache.set_cache(user_id, {'start': True})

            else:
                users.insert_entry({'id': user_id, 'turn_on': True})
                cache.set_cache(user_id, {'start': True})

        except Exception as e:
            print(e)
            raise cherrypy.HTTPRedirect("stats")

        print('был выполнен POST')
        raise cherrypy.HTTPRedirect("stats")

    @cherrypy.expose('upload')
    def upload(self, myFile):
        global TEXTS
        length = int(cherrypy.request.headers['content-length'])
        try:
            data = myFile.file.read(length)
            py_data = russian_xml_data_to_dict(data)

            with open('data.pickle','wb') as file:
                # file.write(data)
                pickle.dump(py_data, file)

            TEXTS = get_texts_from_pickle('data.pickle')

        except Exception as e:

            html = """
            <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Статистика</title>
    <link rel="stylesheet" href="static/css/styles.css">
    <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/e72177d765d6/css/entry/vendor.css">
</head>
<body>
Произошла ошибка при загрузке файла. ({}) Проверьте файл. Он должен быть валидным xml со структурой такого типа<br>
<div class="codehilite"><pre><span></span><span class="nt">&lt;root&gt;</span>
    <span class="c">&lt;!--reject_pay - ответ на &quot;Нет&quot; после прочтения ответа из course_mode_answers --&gt;</span>
    <span class="nt">&lt;reject_pay&gt;</span>Спасибо за проявленный интересик<span class="nt">&lt;/reject_pay&gt;</span>
    <span class="c">&lt;!--manager - выводится при окончания диалога, если пользователь ответил на все &quot;Да&quot; --&gt;</span>
    <span class="nt">&lt;manager&gt;</span>Менеждер свяжется с Вами очень скоро<span class="nt">&lt;/manager&gt;</span>
    <span class="c">&lt;!--give_me_digit - Выводится после начального сообщения, если не ввели цифру --&gt;</span>
    <span class="nt">&lt;give_me_digit&gt;</span>Введите цифру от 1 до 4<span class="nt">&lt;/give_me_digit&gt;</span>
    <span class="c">&lt;!--reject_course - ответ на &quot;Нет&quot; после прочтения ответа из answers --&gt;</span>
    <span class="nt">&lt;reject_course&gt;</span>Спасибо за проявленный интерес, всего доброго<span class="nt">&lt;/reject_course&gt;</span>
    <span class="c">&lt;!-- init - начальный текст--&gt;</span>
    <span class="nt">&lt;init&gt;</span>
        Приветствуем Вас в IT-школе. Про какой курс хотели бы узнать подробнее:
        1. C# (краткое описание возможностей и курса)
        2. Java (краткое описание возможностей и курса)
        3. Frontend (краткое описание возможностей и курса)
        4. Python (краткое описание возможностей и курса)
    <span class="nt">&lt;/init&gt;</span>
    <span class="c">&lt;!--course_mode_answers - массив ответов на слово &quot;Да&quot; после прочтения ответа из answers --&gt;</span>
    <span class="nt">&lt;course_mode_answers&gt;</span>Описание оплаты по C#. У Вас остались еще вопросы?
    <span class="nt">&lt;/course_mode_answers&gt;</span>
    <span class="nt">&lt;course_mode_answers&gt;</span>Описание оплаты по Java. У Вас остались еще вопросы?<span class="nt">&lt;/course_mode_answers&gt;</span>
    <span class="nt">&lt;course_mode_answers&gt;</span>Описание оплаты по Frontend. У Вас остались еще вопросы?<span class="nt">&lt;/course_mode_answers&gt;</span>
    <span class="nt">&lt;course_mode_answers&gt;</span>Описание оплаты по Python. У Вас остались еще вопросы?<span class="nt">&lt;/course_mode_answers&gt;</span>

    <span class="c">&lt;!-- answers - массив ответов на курсы, выбранные в зависимости от цифры--&gt;</span>
    <span class="nt">&lt;answers&gt;</span>Описание курса по C#. Рассказть вам про стоимость курса и условия рассрочки?<span class="nt">&lt;/answers&gt;</span>
    <span class="nt">&lt;answers&gt;</span>Описание курса по Java. Рассказть вам про стоимость курса и условия рассрочки?<span class="nt">&lt;/answers&gt;</span>
    <span class="nt">&lt;answers&gt;</span>Описание курса по Frontend. Рассказть вам про стоимость курса и условия рассрочки?<span class="nt">&lt;/answers&gt;</span>
    <span class="nt">&lt;answers&gt;</span>Описание курса по Python. Рассказть вам про стоимость курса и условия рассрочки?<span class="nt">&lt;/answers&gt;</span>
    <span class="c">&lt;!--yes_or_no - валидация на да/нет --&gt;</span>
    <span class="nt">&lt;yes_or_no&gt;</span>Пожалуйста, выберете Да или Нет<span class="nt">&lt;/yes_or_no&gt;</span>
<span class="nt">&lt;/root&gt;</span>
</pre></div>


 <a href='stats'>Попробовать еще раз </a><br>

</body>
</html>
            """.format(e)
            print(e)
            return html
            # raise cherrypy.HTTPRedirect("stats")

        print('был выполнен POST')
        raise cherrypy.HTTPRedirect("stats")


class Bot(object):
    def __init__(self, token):
        self.session = vk.Session(access_token=token)
        self.api = vk.API(self.session, v='5.53')
        self.__message_handlers = OrderedSet()
        self.turned_on = False

    def add_message_handler(self, handler):
        self.__message_handlers.append(handler)

    def process_message_full(self, cache, message):
        for handler in self.__message_handlers:
            a = handler(self, cache, message)
            if (a):
                self.api.messages.markAsRead(message_ids=[message['id'], ], peer_id=message['user_id'])
                break

    def init_message_handlers(self):
        self.add_message_handler(process_digits)
        self.add_message_handler(process_yes_or_no_course)
        self.add_message_handler(process_yes_or_no_pay)
        self.add_message_handler(process_start_message)
        self.add_message_handler(process_any_message)

    def process_message(self, message, db_messages):
        self.init_message_handlers()
        db_messages.insert_entry(message)

        self.process_message_full(cache, message)

        return 'ok'


if __name__ == '__main__':
    from handlers import *

    cherrypy.config.update({
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8087
    })

    cherrypy.quickstart(BotServer(Bot(TOKEN)), '/teachmeskills/', {

        '/': {
            'tools.staticdir.root': STATIC_ROOT_PATH,
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        },
        '/stats': {
            'tools.auth_basic.on': True,
            'tools.auth_basic.realm': 'localhost',
            'tools.auth_basic.checkpassword': lambda _, user, password: user == STATS_USER and password == STATS_PASS
        }
    })
