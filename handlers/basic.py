import random

from helpers import message_handler, message_handler_standard, message_handler_course_mode_unbind, \
    message_handler_pay_mode_unbind, message_handler_turn, message_handler_start_unbind, get_texts_from_pickle
from run import cache, users


# from run import TEXTS


@message_handler_turn(users, True)
@message_handler_start_unbind(cache)
# @message_handler_course_mode_unbind(cache)
@message_handler_pay_mode_unbind(cache)
@message_handler_standard(message_list=['1', '2', '3', '4'])
def process_digits(self, cache, message):
    TEXTS = get_texts_from_pickle()
    self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                           message=TEXTS['answers'][int(message['body']) - 1])
    cache.set_cache(message['user_id'], {'course_mode': True, 'choice': message['body']})

    return "ok"


@message_handler_turn(users, True)
@message_handler(cache, 'course_mode')
def process_yes_or_no_course(self, cache, message):
    TEXTS = get_texts_from_pickle()
    if message['body'].lower() == 'да':
        choice = cache.get_cache(message['user_id']).get('choice')

        self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                               message=TEXTS['course_mode_answers'][int(choice) - 1])

        cache.set_cache(message['user_id'], {'pay_mode': True, 'course_mode': False})

    elif message['body'].lower() == 'нет':
        self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                               message=TEXTS['reject_course'])

        cache.set_cache(message['user_id'], {'course_mode': False, 'start': True})

    else:
        self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                               message=TEXTS['yes_or_no'])

    return "ok"


@message_handler_turn(users, True)
@message_handler(cache, 'pay_mode')
def process_yes_or_no_pay(self, cache, message):
    TEXTS = get_texts_from_pickle()
    if message['body'].lower() == 'да':
        self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                               message=TEXTS['manager'])

        user_id = int(message['user_id'])
        current_user = users.get_user_by_id(user_id)

        if current_user:
            users.set_turn(current_user['id'], False)
        cache.set_cache(message['user_id'], {'pay_mode': False, 'course_mode': False})

    elif message['body'].lower() == 'нет':
        self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                               message=TEXTS['reject_pay'])

        cache.set_cache(message['user_id'], {'start': True, 'course_mode': False, 'pay_mode': False, })

    else:
        self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000),
                               message=TEXTS['yes_or_no'])

    return "ok"


@message_handler_turn(users, True)
@message_handler(cache, 'start')
def process_start_message(self, cache, message):
    TEXTS = get_texts_from_pickle()
    if not users.get_user_by_id(message['user_id']):
        users.insert_entry({'id': message['user_id'], 'turn_on': True})
    self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000000),
                           message=TEXTS['init'])
    cache.set_cache(message['user_id'], {'turn_on': True, 'start': False})

    return "ok"


@message_handler_start_unbind(cache)
@message_handler_turn(users, True)
def process_any_message(self, cache, message):
    TEXTS = get_texts_from_pickle()
    self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000000),
                           message=TEXTS['give_me_digit'])

    return "ok"
