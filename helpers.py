import json
import os
import pickle

import xmltodict

import texts


def message_handler_turn(mongo_users, turn):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['user_id'] is not None):
                y = mongo_users.get_turned_user_by_id(message['user_id'], turn)
                if y:
                    return handler(self, cache, message)
                else:
                    return None
            else:
                return None
        return wrapped
    return my_decorator


def message_handler(cache, redis_state):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['body'] is not None):
                if cache.get_cache(message['user_id']).get(redis_state):
                    return handler(self, cache, message)
                else:
                    return None
            else:
                return None
        return wrapped
    return my_decorator

def message_handler_standard(message_list=None):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['body'] is not None):
                if (message['body'] in message_list):
                    return handler(self, cache, message)
            else:
                return None
        return wrapped
    return my_decorator

def message_handler_course_mode_unbind(cache):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['body'] is not None):
                if not cache.get_cache(message['user_id']).get('course_mode'):
                    return handler(self, cache, message)
                else:
                    return None
            else:
                return None
        return wrapped
    return my_decorator

def message_handler_pay_mode_unbind(cache):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['body'] is not None):
                if not cache.get_cache(message['user_id']).get('pay_mode'):
                    return handler(self, cache, message)
                else:
                    return None
            else:
                return None
        return wrapped
    return my_decorator

def message_handler_start_unbind(cache):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['body'] is not None):
                if not cache.get_cache(message['user_id']).get('start'):
                    return handler(self, cache, message)
                else:
                    return None
            else:
                return None
        return wrapped
    return my_decorator


def message_handler_func(func=None):
    def my_decorator(handler):
        def wrapped(self, cache, message):
            if (message['body'] is not None):
                if (func(self, cache, message) if func is not None else False):
                    return handler(self, cache, message)
            else:
                return None
        return wrapped
    return my_decorator


def get_texts_from_pickle(filename='data.pickle'):
    if os.path.exists(filename):
        with open(filename, 'rb') as file:
            TEXTS = pickle.load(file)
            TEXTS = TEXTS['root']
    else:
        TEXTS = texts.DEFAULT_COURSE_TEXTS

    return TEXTS


def to_dict(input_ordered_dict):
    return json.loads(json.dumps(input_ordered_dict), encoding='utf-8')


def russian_xml_data_to_dict(data):
    ordered_dict = xmltodict.parse(data)
    return to_dict(ordered_dict)