import json


class RedisCache:
    def __init__(self, connection):
        self.connection = connection

    def set_cache(self, cache_key, data):
        self.connection.set(cache_key, json.dumps(data))

    def get_cache(self, cache_key):
        raw_data = self.connection.get(cache_key)
        if raw_data:
            return json.loads(raw_data.decode('utf-8'))
        return None

    def change_cache_value(self, cache_key, key, value):
        raw_data = self.connection.get(cache_key)
        if raw_data:
            data = json.loads(raw_data.decode('utf-8'))
            data[key] = value
            self.connection.set(cache_key, json.dumps(data))
            return data
        return None

    def get_cache_value(self, cache_key, key):
        raw_data = self.connection.get(cache_key)
        if raw_data:
            data = json.loads(raw_data.decode('utf-8'))
            return data.get(key)
        return None